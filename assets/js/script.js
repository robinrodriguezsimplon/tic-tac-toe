var main = function () {

    var matrice = [
            ['', '', ''],
            ['', '', ''],
            ['', '', '']];
        player1 = "assets/img/cross.png";
        player2 = "assets/img/circle.png";
        players = [ player1, player2 ];
        tour = 0;
        count_gameover = 0;
        grid_items = document.getElementsByClassName('grid-item');
        dialog = document.getElementById('dialog');
        btn_jcj = document.getElementById('jcj');
        btn_jcIA = document.getElementById('jcIA');
        mode = document.getElementById('mode');
        modevalue = 0;
        rightdiv = document.getElementById('right');
        audio = $(".soundclip")[0];
        audio1 = $(".soundclip")[1];
        audio2 = $(".soundclip")[2];
        audio3 = $(".soundclip")[3];
        audio4 = $(".soundclip")[4];   

    $("button").mouseenter(function clickevent() {
        audio.play();
    });
    $("button").click(function() {
        audio1.play();
    });

    majhtml = function() {     
        grid_items[0].innerHTML = (matrice[0][0] == '') ? null : "<img src="+ matrice[0][0] +">";
        grid_items[1].innerHTML = (matrice[0][1] == '') ? null : "<img src="+ matrice[0][1] +">";
        grid_items[2].innerHTML = (matrice[0][2] == '') ? null : "<img src="+ matrice[0][2] +">";
        grid_items[3].innerHTML = (matrice[1][0] == '') ? null : "<img src="+ matrice[1][0] +">";
        grid_items[4].innerHTML = (matrice[1][1] == '') ? null : "<img src="+ matrice[1][1] +">";
        grid_items[5].innerHTML = (matrice[1][2] == '') ? null : "<img src="+ matrice[1][2] +">";
        grid_items[6].innerHTML = (matrice[2][0] == '') ? null : "<img src="+ matrice[2][0] +">";
        grid_items[7].innerHTML = (matrice[2][1] == '') ? null : "<img src="+ matrice[2][1] +">";
        grid_items[8].innerHTML = (matrice[2][2] == '') ? null : "<img src="+ matrice[2][2] +">";
    }

    if (typeof timeoutgameover != 'undefined') {
        clearTimeout(timeoutgameover);
    } 

    jcj = function() {
        mode.innerHTML = "1 vs 1";
        reset();
        if (modevalue == 1) {
            modevalue--;
        }
        btn_jcj.classList.add("animated", "flash");
        rightdiv.classList.add("animated", "bounceInRight");
        setTimeout(() => {
            btn_jcj.classList.remove("animated", "flash");
            rightdiv.classList.remove("animated", "bounceInRight");
        }, 700);  
    }

    jcIA = function() {
        mode.innerHTML = "1 vs IA";
        reset();
        if (modevalue == 0) {
            modevalue++;
        }
        btn_jcIA.classList.add("animated", "flash");
        rightdiv.classList.add("animated", "bounceInRight");
        setTimeout(() => {
            btn_jcIA.classList.remove("animated", "flash");
            rightdiv.classList.remove("animated", "bounceInRight");
        }, 700);  
    }

    rdmIA = function() {
        var x = Math.floor(Math.random() * 3);
            return x;
    }
    
    game = function() {
        r = rdmIA();
        r2 = rdmIA();
        if (modevalue == 1 && matrice[r][r2] == '') {            
            matrice[r].splice(r2 , 1, player2);
            count_gameover++;
            } else if ((modevalue == 1 && matrice[r][r2] == player1) || (modevalue == 1 && matrice[r][r2] == player2)) {
                timeoutgameover = setTimeout(() => {
                game();
            }, 10);   
        }
        tourfct();
        majhtml();
        gameover();
        win();
    }

    tourfct = function() {
        if (tour == 0 && modevalue == 0) {
            tour++;
        } else if (tour == 1 && modevalue == 0) {
            tour--;
        } 
    }

    gameover = function() {
        if (count_gameover > 8 && win() != true) {
            
            swal({
                title: "Match nul!",
                button: "Play again"
            }); 
            audio4.play();
            reset();
            clearTimeout(timeoutgameover);
        } 
    }

    win = function() {
        for ( let playerIndex in players ) {
            if (( matrice[0][0] === players[ playerIndex ] && matrice[1][0] === players[ playerIndex ] && matrice[2][0] === players[ playerIndex ] ) ||
                ( matrice[0][1] === players[ playerIndex ] && matrice[1][1] === players[ playerIndex ] && matrice[2][1] === players[ playerIndex ] ) ||
                ( matrice[0][2] === players[ playerIndex ] && matrice[1][2] === players[ playerIndex ] && matrice[2][2] === players[ playerIndex ] ) || 
                ( matrice[0][0] === players[ playerIndex ] && matrice[0][1] === players[ playerIndex ] && matrice[0][2] === players[ playerIndex ] ) ||
                ( matrice[1][0] === players[ playerIndex ] && matrice[1][1] === players[ playerIndex ] && matrice[1][2] === players[ playerIndex ] ) ||
                ( matrice[2][0] === players[ playerIndex ] && matrice[2][1] === players[ playerIndex ] && matrice[2][2] === players[ playerIndex ] ) ||
                ( matrice[0][0] === players[ playerIndex ] && matrice[1][1] === players[ playerIndex ] && matrice[2][2] === players[ playerIndex ] ) ||
                ( matrice[0][2] === players[ playerIndex ] && matrice[1][1] === players[ playerIndex ] && matrice[2][0] === players[ playerIndex ] )) {
                
                swal({
                    title: "Won the game, GG.",
                    icon: players[ playerIndex ],
                    button: "Play again"
                }); 
                audio3.play();
                reset();
                return true;
            }
        }
    }
/*
    game = function() {

        r = rdmIA();
        r2 = rdmIA();
                        } else if (modevalue == 1 && matrice[r][r2] == '') {
                            matrice[r].splice(r2 , 1, player2);
                            count_gameover++;
                            } if ((modevalue == 1 && matrice[r][r2] == player1) || (modevalue == 1 && matrice[r][r2] == player2)) {
                                timeoutgameover = setTimeout(() => {
                                game();
                                }, 10);   
                            }
        tourfct();
        majhtml();
        gameover();
        win();
    }
*/
    reset = function() {
        [].slice.call( grid_items ).forEach(function ( grid_items ) {
            grid_items.innerHTML = '';
        });
        matrice = [
            ['', '', ''],
            ['', '', ''],
            ['', '', '']];
        count_gameover = 0;
        tour = 0;
    }
    
    grid_items[0].addEventListener('click', ()=> {
        if (matrice[0][0] == '') {
            matrice[0].splice(0, 1, players[tour]); 
            count_gameover++;
            game();      
        } else {
            audio2.play();
        }
    });

    grid_items[1].addEventListener('click', ()=> {
        if (matrice[0][1] == '') {
            matrice[0].splice(1, 1, players[tour]);
            count_gameover++;
            game();      
        } else {
            audio2.play();
        }
    });

    grid_items[2].addEventListener('click', ()=> {
        if (matrice[0][2] == '') {
            matrice[0].splice(2, 1, players[tour]);
            count_gameover++;
            game();       
        } else {
            audio2.play();
        }
    });

    grid_items[3].addEventListener('click', ()=> {
        if (matrice[1][0] == '') {
            matrice[1].splice(0, 1, players[tour]);
            count_gameover++;
            game();       
        } else {
            audio2.play();
        }
    });

    grid_items[4].addEventListener('click', ()=> {
        if (matrice[1][1] == '') {
            matrice[1].splice(1, 1, players[tour]);
            count_gameover++;
            game();       
        } else {
            audio2.play();
        }
    });

    grid_items[5].addEventListener('click', ()=> {
        if (matrice[1][2] == '') {
            matrice[1].splice(2, 1, players[tour]);
            count_gameover++;
            game();       
        } else {
            audio2.play();
        }
    });

    grid_items[6].addEventListener('click', ()=> {
        if (matrice[2][0] == '') {
            matrice[2].splice(0, 1, players[tour]);
            count_gameover++;
            game();       
        } else {
            audio2.play();
        }
    });

    grid_items[7].addEventListener('click', ()=> {
        if (matrice[2][1] == '') {
            matrice[2].splice(1, 1, players[tour]);
            count_gameover++;
            game();       
        } else {
            audio2.play();
        }
    });

    grid_items[8].addEventListener('click', ()=> {
        if (matrice[2][2] == '') {
            matrice[2].splice(2, 1, players[tour]);
            count_gameover++;
            game();       
        } else {
            audio2.play();
        }
    });

    btn_jcj.addEventListener('click', jcj);
    btn_jcIA.addEventListener('click', jcIA);
}
document.addEventListener('DOMContentLoaded', main);



  
/*
    matrice[0][0] && matrice[0][1] ? players[ playerIndex ] : matrice[0].splice(2 , 1, player2)
    matrice[0][0] && matrice[0][2] ? players[ playerIndex ] : matrice[0].splice(1 , 1, player2)
    matrice[0][1] && matrice[0][2] ? players[ playerIndex ] : matrice[0].splice(0 , 1, player2)


    (matrice[0][1] === players[ playerIndex ] && matrice[1][1] === players[ playerIndex ] && matrice[2][1] === players[ playerIndex ]) ||
    (matrice[0][2] === players[ playerIndex ] && matrice[1][2] === players[ playerIndex ] && matrice[2][2] === players[ playerIndex ]) || 
    (matrice[0][0] === players[ playerIndex ] && matrice[0][1] === players[ playerIndex ] && matrice[0][2] === players[ playerIndex ]) ||
    (matrice[1][0] === players[ playerIndex ] && matrice[1][1] === players[ playerIndex ] && matrice[1][2] === players[ playerIndex ]) ||
    (matrice[2][0] === players[ playerIndex ] && matrice[2][1] === players[ playerIndex ] && matrice[2][2] === players[ playerIndex ]) ||
    (matrice[0][0] === players[ playerIndex ] && matrice[1][1] === players[ playerIndex ] && matrice[2][2] === players[ playerIndex ]) ||
    (matrice[0][2] === players[ playerIndex ] && matrice[1][1] === players[ playerIndex ] && matrice[2][0] === players[ playerIndex ])) {}

    */

